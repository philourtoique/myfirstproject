USE ROLE ACCOUNTADMIN;
USE WAREHOUSE COMPUTE_WH;
USE DATABASE DEMO_DB;
USE SCHEMA PUBLIC;

-- CHANGE DATA CAPTURE FROM A STREAM OF STAGE TABLE - STG_PINS_POSITION_INVEST - 
CREATE OR REPLACE TASK LOAD_PINS_POSITION_INVEST
    WAREHOUSE = compute_wh
    SCHEDULE = '1 MINUTE'
    WHEN SYSTEM$STREAM_HAS_DATA('DEMO_DB.PUBLIC.PINS_POSITION_INVEST_STRM')
AS
MERGE INTO PINS_POSITION_INVEST PI
USING pins_position_invest_strm PIS
ON  PI.c_ident_evlp_instm = PIS.c_ident_evlp_instm and 
    PI.v_evlp_instm = PIS.v_evlp_instm and
    PI.c_type_instm = PIS.c_type_instm and
    PI.l_type_instm = PIS.l_type_instm and
    PI.c_ident_tech_transac_invst = PIS.c_ident_tech_transac_invst and
    PI.d_deb_vali_posi = PIS.d_deb_vali_posi
WHEN MATCHED AND PIS.METADATA$ACTION = 'INSERT' THEN
    UPDATE SET  PI.d_fin_vali_posi = PIS.d_fin_vali_posi, 
                PI.m_posi_quantt = PIS.m_posi_quantt, 
                PI.m_posi_net_invst_devi_instm = PIS.m_posi_net_invst_devi_instm,
                PI.d_update = current_timestamp()
WHEN NOT MATCHED AND PIS.METADATA$ACTION = 'INSERT' THEN
    INSERT (PI.c_ident_evlp_instm, PI.v_evlp_instm, PI.c_type_instm, PI.l_type_instm, PI.c_ident_tech_transac_invst, PI.d_deb_vali_posi, PI.d_fin_vali_posi, PI.m_posi_quantt, PI.m_posi_net_invst_devi_instm, PI.d_update)
    VALUES (PIS.c_ident_evlp_instm, PIS.v_evlp_instm, PIS.c_type_instm, PIS.l_type_instm, PIS.c_ident_tech_transac_invst, PIS.d_deb_vali_posi, PIS.d_fin_vali_posi, PIS.m_posi_quantt, PIS.m_posi_net_invst_devi_instm, current_timestamp());

ALTER TASK LOAD_PINS_POSITION_INVEST SUSPEND;

-- FORECAST - UPDATE POSITION AMOUNTS FOR ACTIONS
CREATE OR REPLACE TASK UPD_ACTION
    WAREHOUSE = compute_wh
    AFTER LOAD_PINS_POSITION_INVEST
AS
    UPDATE PINS_POSITION_INVEST 
        SET m_posi_net_invst_devi_instm = m_posi_net_invst_devi_instm * 1.1
    WHERE c_type_instm = 'ACTION';

ALTER TASK UPD_ACTION SUSPEND;

-- FORECAST - UPDATE POSITION AMOUNTS FOR OBLIGATIONS
CREATE OR REPLACE TASK UPD_OBLIGATION
    WAREHOUSE = compute_wh
    AFTER LOAD_PINS_POSITION_INVEST
AS
    UPDATE PINS_POSITION_INVEST 
        SET m_posi_net_invst_devi_instm = m_posi_net_invst_devi_instm * 0.8
    WHERE c_type_instm = 'OC';

ALTER TASK UPD_OBLIGATION SUSPEND;

-- FORECAST - UPDATE POSITION AMOUNTS FOR SHARES
CREATE OR REPLACE TASK UPD_SHARE
    WAREHOUSE = compute_wh
    AFTER LOAD_PINS_POSITION_INVEST
AS
    UPDATE PINS_POSITION_INVEST 
        SET m_posi_net_invst_devi_instm = m_posi_net_invst_devi_instm * 1.2
    WHERE c_type_instm = 'SHARE';

ALTER TASK UPD_SHARE SUSPEND;

-- turn on tasks
ALTER TASK UPD_ACTION RESUME;

ALTER TASK UPD_OBLIGATION RESUME;

ALTER TASK UPD_SHARE RESUME;

ALTER TASK LOAD_PINS_POSITION_INVEST RESUME;
