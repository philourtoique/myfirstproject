USE ROLE ACCOUNTADMIN;
USE WAREHOUSE COMPUTE_WH;
USE DATABASE DEMO_DB;

SELECT c_ent
FROM PUBLIC.ETL_PERIM_PART_INVEST;

SELECT c_ent
FROM ETL_PERIM_PART_INVEST
GROUP BY c_detailed;

SELECT c_ent, COUNT(1)
from ETL_PERIM_PART_INVEST
GROUP BY c_detailed;

select 
from PUBLIC.ETL_PERIM_PART_INVEST


select count(1) 
from PINS_POSITION_INVEST;

select count(1) 
from STG_PINS_POSITION_INVEST;


select *
from PINS_POSITION_INVEST;


select c_type_instm, l_type_instm
        ,sum(m_posi_net_invst_devi_instm) as m_total_posi_net_invst
from PINS_POSITION_INVEST
group by c_type_instm, l_type_instm;

create or replace TRANSIENT TABLE CALC_POSITIONS_BY_TYPE As
select c_type_instm, l_type_instm
        ,sum(m_posi_net_invst_devi_instm) as m_total_posi_net_invst
from PINS_POSITION_INVEST
group by c_type_instm, l_type_instm;


create or replace TRANSIENT TABLE AGG_POSITIONS_BY_TYPE As
select c_type_instm, l_type_instm
        ,sum(m_posi_net_invst_devi_instm) as m_total_posi_net_invst
from PINS_POSITION_INVEST
group by c_type_instm, l_type_instm;

DROP TABLE AGG_POSITIONS_BY_TYPE;

select * from AGG_POSITIONS_BY_TYPE;




select *
from table(information_schema.task_dependents(task_name => 'LOAD_PINS_POSITION_INVEST', recursive => TRUE));


WITH depWKF AS
    (    select name
        from table(information_schema.task_dependents(task_name => 'LOAD_PINS_POSITION_INVEST', recursive => TRUE))
    ),
    execWKF AS
    (   select name, state, scheduled_time,
                case 
                    when  state ='FAILED' THEN 1 
                    when  state ='CANCELLED' THEN 1
                    else 0
                end as out_for_error,
                case 
                    when  state ='SUCCEEDED' THEN 1
                    else 0
                end as ind_for_success,
                (select count(1) from depWKF) as nb_tasks
        from table(information_schema.task_history())
        where scheduled_time >= dateadd(hour,-2, CURRENT_TIMESTAMP())
        and name in (select name from depWKF)
    ),
    indWKF AS
    (   select  max(nb_tasks) as nb_tasks,
                sum(out_for_error) as nb_successed_tasks,
                sum(out_for_error) as nb_error
        from execWKF
    )
select  CASE 
            WHEN nb_error > 0 then 2
            WHEN nb_successed_tasks = nb_tasks THEN 1
            else 0
        end as ind_out_wkf
from indWKF;


select *
  from table(information_schema.task_history());


  call SP_LAUNCH_SUB_WORKFLOW('CALC_POSITIONS_BY_TYPE');


  SELECT max(SCHEDULED_TIME) 
  FROM TABLE(ACCOUNTADMIN.task_history(task_name=>'LOAD_PINS_POSITION_INVEST'));

select to_timestamp('Sun Jul 18 2021 07:22:17 GMT-0700 (Pacific Daylight Time)');

TRUNCATE TABLE STG_PINS_POSITION_INVEST;

TRUNCATE TABLE PINS_POSITION_INVEST;

select *
  from table(information_schema.task_history())
--where NAME like 'LOAD%'
  order by scheduled_time desc;