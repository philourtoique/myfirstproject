USE ROLE ACCOUNTADMIN;
USE WAREHOUSE COMPUTE_WH;
USE DATABASE DEMO_DB;
USE SCHEMA PUBLIC;


CREATE OR REPLACE PROCEDURE SP_LAUNCH_SUB_WORKFLOW(ROOT_TASK_NAME VARCHAR)
RETURNS STRING
LANGUAGE javascript
execute as caller
AS
$$ 
    // Initialize variables
    var task_name = ROOT_TASK_NAME;
    var task_name2 = ROOT_TASK_NAME;
    var return_value = "";
    //return task_name

    // Start the workflow 
    var stmt = snowflake.createStatement({sqlText: `
        ALTER TASK IDENTIFIER(:1) SUSPEND
    `, binds:[task_name]});
    res = stmt.execute()


    // Retrieve the start date of the workflow 
    var stmt = snowflake.createStatement({sqlText: `
        SELECT max(SCHEDULED_TIME) FROM TABLE(information_schema.TASK_HISTORY(task_name=>:1)) 
    `, binds:[task_name]});
    res = stmt.execute()
    res.next();

    //return res.getColumnValue(1)

    var sql_cmd = " SELECT * \
                    FROM table(information_schema.task_dependents(task_name => '"+task_name+"', recursive => TRUE))"
    var stmt = snowflake.createStatement({sqlText: sql_cmd});
    res = stmt.execute()
    res.next();

    //return res.getColumnValue(1)

    var sql_cmd = "  WITH depWKF AS \
            (   select name \
                from table(information_schema.task_dependents(task_name => '"+task_name+"', recursive => TRUE)) \
            ), \
            execWKF AS \
            (   select name, state, scheduled_time, \
                        case \
                            when  state ='FAILED' THEN 1 \
                            when  state ='CANCELLED' THEN 1 \
                            else 0 \
                        end as out_for_error, \
                        case \
                            when  state ='SUCCEEDED' THEN 1 \
                            else 0 \
                        end as ind_for_success, \
                        (select count(1) from depWKF) as nb_tasks \
                from table(information_schema.task_history()) \
                where scheduled_time >= (SELECT max(SCHEDULED_TIME) FROM TABLE(information_schema.TASK_HISTORY(task_name=>'"+task_name+"')))  \
                and name in (select name from depWKF) \
            ), \
            indWKF AS \
            (   select  max(nb_tasks) as nb_tasks, \
                        sum(out_for_error) as nb_successed_tasks, \
                        sum(out_for_error) as nb_error \
                from execWKF \
            ) \
        select  CASE \
                    WHEN nb_error > 0 then 2 \
                    WHEN nb_successed_tasks = nb_tasks THEN 1 \
                    else 0 \
                end as ind_out_wkf \
        from indWKF; "
    var stmt = snowflake.createStatement({sqlText: sql_cmd});
    res = stmt.execute()
    res.next();

    return res.getColumnValue(1)   
    
    
    // Retrieve the start date of the workflow 
    var stmt = snowflake.createStatement({sqlText: `
        SELECT * FROM table(information_schema.task_dependents(task_name => :1, recursive => TRUE))
    `, binds:[task_name2]});
    res = stmt.execute()
    res.next();

    return res.getColumnValue(1)

 

    return res.getColumnValue(1) 


    // Check the state tasks of the workflow
    var stmt = snowflake.createStatement({sqlText: `
        WITH depWKF AS
            (   select name
                from table(information_schema.task_dependents(task_name => 'CALC_POSITIONS_BY_TYPE', recursive => TRUE))
            ),
            execWKF AS
            (   select name, state, scheduled_time,
                        case 
                            when  state ='FAILED' THEN 1 
                            when  state ='CANCELLED' THEN 1
                            else 0
                        end as out_for_error,
                        case 
                            when  state ='SUCCEEDED' THEN 1
                            else 0
                        end as ind_for_success,
                        (select count(1) from depWKF) as nb_tasks
                from table(information_schema.task_history())
                where scheduled_time >= (SELECT max(SCHEDULED_TIME) FROM TABLE(information_schema.TASK_HISTORY(task_name=>:1))) 
                and name in (select name from depWKF)
            ),
            indWKF AS
            (   select  max(nb_tasks) as nb_tasks,
                        sum(out_for_error) as nb_successed_tasks,
                        sum(out_for_error) as nb_error
                from execWKF
            )
        select  CASE 
                    WHEN nb_error > 0 then 2
                    WHEN nb_successed_tasks = nb_tasks THEN 1
                    else 0
                end as ind_out_wkf
        from indWKF;
    `, binds:[task_name]});
    res = stmt.execute()
    //res.next();

    while (res.next())  {
        return_value += "\n";
        return_value += res.getColumnValue(1);     
    }

    return return_value;
$$

call SP_LAUNCH_SUB_WORKFLOW('CALC_POSITIONS_BY_TYPE');
