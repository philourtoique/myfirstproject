USE ROLE ACCOUNTADMIN;
USE WAREHOUSE COMPUTE_WH;
USE DATABASE DEMO_DB;
USE SCHEMA PUBLIC;

CREATE OR REPLACE STREAM pins_position_invest_strm 
    ON TABLE STG_PINS_POSITION_INVEST
    APPEND_ONLY = TRUE;