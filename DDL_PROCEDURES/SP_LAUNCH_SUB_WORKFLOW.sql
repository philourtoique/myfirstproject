USE ROLE ACCOUNTADMIN;
USE WAREHOUSE COMPUTE_WH;
USE DATABASE DEMO_DB;
USE SCHEMA PUBLIC;


CREATE OR REPLACE PROCEDURE SP_LAUNCH_SUB_WORKFLOW(WKF_NAME VARCHAR, ROOT_TASK_NAME VARCHAR)
RETURNS STRING
LANGUAGE javascript
execute as caller
AS
$$ 
    // Initialize variables
    var task_name = ROOT_TASK_NAME;
    var vtimeout = 100000;
    var return_value = "";
    //return task_name

    // Function specifications 
    function sleep(milliseconds) {
        const date = Date.now();
        let currentDate = null;
        do {
        currentDate = Date.now();
        } while (currentDate - date < milliseconds);
    };

    function getTimestamp() {
        const pad = (n,s=2) => (`${new Array(s).fill(0)}${n}`).slice(-s);
        const d = new Date();
        return `${pad(d.getFullYear(),4)}-${pad(d.getMonth()+1)}-${pad(d.getDate())} ${pad(d.getHours())}:${pad(d.getMinutes())}:${pad(d.getSeconds())}`;
    }

    // Start the workflow 
    var stmt = snowflake.createStatement({sqlText: `
        ALTER TASK IDENTIFIER(:1) RESUME
    `, binds:[task_name]});
    var res = stmt.execute()
    var startTS = Date.now();
    return_value = "Start the workflow "+ WKF_NAME +" at " + getTimestamp()


    // save de start date 
    var sql_cmd = " CREATE OR REPLACE TRANSIENT TABLE LAUNCH_ST_"+task_name+" AS SELECT max(SCHEDULED_TIME) as STARTTIME \
                    FROM TABLE(information_schema.TASK_HISTORY(task_name=>'"+task_name+"'))";
    stmt = snowflake.createStatement({sqlText: sql_cmd});
    res = stmt.execute();
    res.next();

   
    sql_cmd = "  WITH depWKF AS \
            (   select name \
                from table(information_schema.task_dependents(task_name => '"+task_name+"', recursive => TRUE)) \
            ), \
            execWKF AS \
            (   select name, state, scheduled_time, \
                        case \
                            when  state ='FAILED' THEN 1 \
                            when  state ='CANCELLED' THEN 1 \
                            else 0 \
                        end as out_for_error, \
                        case \
                            when  state ='SUCCEEDED' THEN 1 \
                            else 0 \
                        end as ind_for_success, \
                        (select count(1) from depWKF) as nb_tasks \
                from table(information_schema.task_history()) \
                where scheduled_time >= (SELECT max(STARTTIME) FROM LAUNCH_ST_"+task_name+" )  \
                and name in (select name from depWKF) \
            ), \
            indWKF AS \
            (   select  max(nb_tasks) as nb_tasks, \
                        sum(ind_for_success) as nb_successed_tasks, \
                        sum(out_for_error) as nb_error \
                from execWKF \
            ) \
        select  CASE \
                    WHEN nb_error > 0 then 2 \
                    WHEN nb_successed_tasks = nb_tasks THEN 1 \
                    else 0 \
                end as ind_out_wkf \
        from indWKF; "
    stmt = snowflake.createStatement({sqlText: sql_cmd});
    res = stmt.execute();
    res.next();

    var wkf_return_code = res.getColumnValue(1);   
    

    //return res.getColumnValue(1)

    var incr = 1;
    var durationMS = Date.now() - startTS;
    //return durationMS
    return_value += "\n";
    return_value += getTimestamp()+ " - n"+incr+" return code = "+ wkf_return_code;

    while (wkf_return_code == 0 && durationMS < vtimeout) {
        incr +=1;
        sleep(1500);
        stmt = snowflake.createStatement({sqlText: sql_cmd});
        res = stmt.execute();
        res.next();
        wkf_return_code = res.getColumnValue(1); 
        return_value += "\n";
        return_value += getTimestamp()+" - n"+incr+" return code = "+ wkf_return_code;
        durationMS = Date.now() - startTS;  
    }

    return_value += "\n";
    return_value += getTimestamp()+" - last return code = "+ wkf_return_code;
    if ( wkf_return_code == 1 ) {
        return_value += getTimestamp()+" - last return code = "+ wkf_return_code +" successfully terminated ";
    } else if ( wkf_return_code == 2 ) {
        return_value += getTimestamp()+" - last return code = "+ wkf_return_code +" terminated successfully with errors ";
    } else {
        return_value += getTimestamp()+" - last return code = "+ wkf_return_code +" timeout occurred, task has not been completed ";
    }

    // Stop the workflow 
    stmt = snowflake.createStatement({sqlText: `
        ALTER TASK IDENTIFIER(:1) SUSPEND
    `, binds:[task_name]});
    res = stmt.execute()

    // save the tempory table 
    sql_cmd = " DROP TABLE LAUNCH_ST_"+task_name+"; ";
    stmt = snowflake.createStatement({sqlText: sql_cmd});
    res = stmt.execute();
    res.next();

    return_value += "\n";
    return_value += "Stop the workflow "+ WKF_NAME +" at " + getTimestamp()

    return return_value;
$$

--call SP_LAUNCH_SUB_WORKFLOW('POSITIONS FLOW','CALC_POSITIONS_BY_TYPE');
