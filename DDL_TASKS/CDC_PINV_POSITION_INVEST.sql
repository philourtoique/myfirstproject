USE ROLE ACCOUNTADMIN;
USE WAREHOUSE COMPUTE_WH;
USE DATABASE DEMO_DB;
USE SCHEMA PUBLIC;

-- CHANGE DATA CAPTURE FROM A STREAM OF STAGE TABLE - STG_PINS_POSITION_INVEST - 
CREATE OR REPLACE TASK LOAD_PINS_POSITION_INVEST
    WAREHOUSE = compute_wh
    SCHEDULE = '1 MINUTE'
    WHEN SYSTEM$STREAM_HAS_DATA('DEMO_DB.PUBLIC.PINS_POSITION_INVEST_STRM')
AS
MERGE INTO PINS_POSITION_INVEST PI
USING pins_position_invest_strm PIS
ON  PI.c_ident_evlp_instm = PIS.c_ident_evlp_instm and 
    PI.v_evlp_instm = PIS.v_evlp_instm and
    PI.c_type_instm = PIS.c_type_instm and
    PI.l_type_instm = PIS.l_type_instm and
    PI.c_ident_tech_transac_invst = PIS.c_ident_tech_transac_invst and
    PI.d_deb_vali_posi = PIS.d_deb_vali_posi
WHEN MATCHED AND PIS.METADATA$ACTION = 'INSERT' THEN
    UPDATE SET  PI.d_fin_vali_posi = PIS.d_fin_vali_posi, 
                PI.m_posi_quantt = PIS.m_posi_quantt, 
                PI.m_posi_net_invst_devi_instm = PIS.m_posi_net_invst_devi_instm,
                PI.d_update = current_timestamp()
WHEN NOT MATCHED AND PIS.METADATA$ACTION = 'INSERT' THEN
    INSERT (PI.c_ident_evlp_instm, PI.v_evlp_instm, PI.c_type_instm, PI.l_type_instm, PI.c_ident_tech_transac_invst, PI.d_deb_vali_posi, PI.d_fin_vali_posi, PI.m_posi_quantt, PI.m_posi_net_invst_devi_instm, PI.d_update)
    VALUES (PIS.c_ident_evlp_instm, PIS.v_evlp_instm, PIS.c_type_instm, PIS.l_type_instm, PIS.c_ident_tech_transac_invst, PIS.d_deb_vali_posi, PIS.d_fin_vali_posi, PIS.m_posi_quantt, PIS.m_posi_net_invst_devi_instm, current_timestamp());

ALTER TASK LOAD_PINS_POSITION_INVEST RESUME;