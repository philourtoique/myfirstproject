USE ROLE ACCOUNTADMIN;
USE WAREHOUSE COMPUTE_WH;
USE DATABASE DEMO_DB;
USE SCHEMA PUBLIC;

CREATE OR REPLACE TABLE public.STG_PINS_POSITION_INVEST (
	c_ident_evlp_instm              VARCHAR(50) NULL,
	v_evlp_instm                    VARCHAR(200) NULL,
	c_type_instm                    VARCHAR(50) NULL,
	l_type_instm                    VARCHAR(200) NULL,
	c_ident_tech_transac_invst      VARCHAR(100) NULL,
	d_deb_vali_posi                 DATETIME NULL,
	d_fin_vali_posi                 DATETIME NULL,
	m_posi_quantt                   NUMBER(38,4) NULL,
	m_posi_net_invst_devi_instm     NUMBER(38,4) NULL,
    filename                        STRING  NOT NULL,
    file_row_seq                    NUMBER  NOT NULL, 
    ldts                            TIMESTAMP  NOT NULL, 
    rscr                            STRING  NOT NULL
);
 