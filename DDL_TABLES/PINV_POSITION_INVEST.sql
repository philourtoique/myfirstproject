USE ROLE ACCOUNTADMIN;
USE WAREHOUSE COMPUTE_WH;
USE DATABASE DEMO_DB;
USE SCHEMA PUBLIC;

CREATE OR REPLACE TABLE public.PINS_POSITION_INVEST (
	c_ident_evlp_instm              VARCHAR(50) NULL,
	v_evlp_instm                    VARCHAR(200) NULL,
	c_type_instm                    VARCHAR(50) NULL,
	l_type_instm                    VARCHAR(200) NULL,
	c_ident_tech_transac_invst      VARCHAR(100) NULL,
	d_deb_vali_posi                 DATETIME NULL,
	d_fin_vali_posi                 DATETIME NULL,
	m_posi_quantt                   NUMBER(38,4) NULL,
	m_posi_net_invst_devi_instm     NUMBER(38,4) NULL,
    d_update                        DATETIME
);
 
--select count(1) from PINS_POSITION_INVEST;

--truncate table PINS_POSITION_INVEST;

--select *
--from table(information_schema.copy_history(table_name=>'PINS_POSITION_INVEST', start_time=> dateadd(hours, -1, current_timestamp())));


