USE ROLE ACCOUNTADMIN;
USE WAREHOUSE COMPUTE_WH;
USE DATABASE DEMO_DB;
USE SCHEMA PUBLIC;

create stage azSnowStage
  storage_integration = IntegrationAzBlogStorage
  url = 'azure://azstorageacc4snow.blob.core.windows.net/myfiles/snow/'
  file_format = my_csv_format;