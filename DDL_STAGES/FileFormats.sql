USE ROLE ACCOUNTADMIN;
USE WAREHOUSE COMPUTE_WH;
USE DATABASE DEMO_DB;
USE SCHEMA PUBLIC;

create or replace file format my_csv_format
  type = csv
  field_delimiter = ';'
  skip_header = 1;