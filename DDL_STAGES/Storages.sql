-- Azure CLI Scrapbook > https://github.com/Microsoft/vscode-azurecli
-- Procédure : https://docs.snowflake.com/en/user-guide/data-load-azure-config.html (https://docs.snowflake.com/en/user-guide/data-load-azure.html)

-- Purpose : Bulk Loading from Microsoft Azure
--           Configuring an Azure Container for Loading Data

-- [operation in Snowflake]
-- Step 1: Create a Cloud Storage Integration in Snowflake

USE ROLE ACCOUNTADMIN;
USE WAREHOUSE COMPUTE_WH;
USE DATABASE DEMO_DB;
USE SCHEMA PUBLIC;

-- [operation in Azure]
-- Allowing the VNet Subnet IDs 
select system$get_snowflake_platform_info();
-- {"snowflake-vpc-id":["vpc-10494c79"]}
-- dans le portail AZURE > commande suivante
-- az storage account network-rule add --account-name csb872749228899x4faax94f --resource-group cloud-shell-storage-westeurope --subnet "{"snowflake-vpc-id":["vpc-10494c79"]}"

-- Step 1: Create a Cloud Storage Integration in Snowflake
CREATE STORAGE INTEGRATION IntegrationAzBlogStorage
  TYPE = EXTERNAL_STAGE
  STORAGE_PROVIDER = AZURE
  ENABLED = TRUE
  AZURE_TENANT_ID = 'adba5c92-c4b1-470f-929b-1d553c954fec'
  STORAGE_ALLOWED_LOCATIONS = ('azure://azstorageacc4snow.blob.core.windows.net/myfiles/snow/');


desc storage integration IntegrationAzBlogStorage;

/*
register Snowflake in Active Directory with URL in a web browser
{
  "property": "AZURE_CONSENT_URL",
  "property_type": "String",
  "property_value": "https://login.microsoftonline.com/adba5c92-c4b1-470f-929b-1d553c954fec/oauth2/authorize?client_id=95fac833-f649-42b1-9962-5cc91b22210d&response_type=code",
  "property_default": ""
}
*/

/* 
  access Control (IAM) > Storage Blob Data Contributor 
{
  "property": "AZURE_MULTI_TENANT_APP_NAME",
  "property_type": "String",
  "property_value": "SnowflakePACInt0130_1625749722932",
  "property_default": ""
}
*/

-- [manual operation in Azure]
-- Step 2: Grant Snowflake Access to the Storage Locations
--   < desc notification integration <integration_name; > 
--   < to register Snowflake in Active Directory from AZURE_CONSENT_URL >
--   < to access Control (IAM) » Add role assignment on a Storage from AZURE_MULTI_TENANT_APP_NAME >
--   https://docs.snowflake.com/en/user-guide/data-load-azure-config.html#step-2-grant-snowflake-access-to-the-storage-locations

