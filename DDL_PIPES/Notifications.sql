USE ROLE ACCOUNTADMIN;
USE WAREHOUSE COMPUTE_WH;
USE DATABASE DEMO_DB;
USE SCHEMA PUBLIC;

create notification integration aznotificationint
  enabled = true
  type = queue
  notification_provider = azure_storage_queue
  azure_storage_queue_primary_uri = 'https://azstorageaccqueue4snow.queue.core.windows.net/azstoragqueue4snow'
  azure_tenant_id = 'adba5c92-c4b1-470f-929b-1d553c954fec';


-- desc notification integration aznotificationint;

/*
register Snowflake in Active Directory with URL in a web browser
{
  "property": "AZURE_CONSENT_URL",
  "property_type": "String",
  "property_value": "https://login.microsoftonline.com/adba5c92-c4b1-470f-929b-1d553c954fec/oauth2/authorize?client_id=2b9e6acc-4843-4b25-bded-7b5f80f05ac9&response_type=code",
  "property_default": ""
}
*/
