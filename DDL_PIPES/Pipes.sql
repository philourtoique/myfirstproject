USE ROLE ACCOUNTADMIN;
USE WAREHOUSE COMPUTE_WH;
USE DATABASE DEMO_DB;
USE SCHEMA PUBLIC;

-- BULK LOADING FROM AZURE CONTAINER - Investment positions - 
create or replace pipe demo_db.public.pipepositioninvest
  auto_ingest = true
  integration = 'AZNOTIFICATIONINT'
  as
  copy into demo_db.public.STG_PINS_POSITION_INVEST
  from 
  ( 
    SELECT $1,$2,$3,$4,$5,$6,$7,$8,$9
        , metadata$filename
        , metadata$file_row_number
        , CURRENT_TIMESTAMP()
        , 'Investment positions'
    FROM @demo_db.public.azSnowStage
  ); 

/*
  copy into demo_db.public.PINS_POSITION_INVEST
  from @demo_db.public.azSnowStage
  file_format = my_csv_format;
*/


ALTER PIPE demo_db.public.pipepositioninvest REFRESH;


--select system$pipe_status('public.pipepositioninvest');

/*
{
  "SYSTEM$PIPE_STATUS('PUBLIC.PIPEPOSITIONINVEST')":
    "{\"executionState\":\"RUNNING\",
      \"pendingFileCount\":0,
      \"notificationChannelName\":\"https://azstorageaccqueue4snow.queue.core.windows.net/azstoragqueue4snow\",
      \"numOutstandingMessagesOnChannel\":1,
      \"lastReceivedMessageTimestamp\":\"2021-07-10T22:20:15.26Z\",
      \"lastForwardedMessageTimestamp\":\"2021-07-10T22:20:15.322Z\"}"
}
*/

--  SHOW PIPES;

--  SHOW STAGES;

-- SHOW NOTIFICATIONS;

